import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    data: []
  },
  mutations: {
    setData(state, data) {
      state.data = data;
    }
  },
  actions: {
    getData: async function ({ commit }) {
      try{
        const response = await axios.get('http://localhost:3000/data');
        commit('setData', response.data);
      }catch(e){
        console.log("JSON ERROR: ", e);
      }
    }
  },
  modules: {
  }
})
